//src/components/Navbar/index.js

import React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { NavLink } from 'react-router-dom';

const Navbar = () => (
  <AppBar position="static">
    <Toolbar>
      <Typography variant="h6" sx={{ flexGrow: 1 }}>
        Resume Builder
      </Typography>
      <Button color="inherit" component={NavLink} to="/" end>Home</Button>
      <Button color="inherit" component={NavLink} to="/fill-details">Fill Details</Button>
      <Button color="inherit" component={NavLink} to="/preview">Preview</Button>
      <Button color="inherit" component={NavLink} to="/about-us">About Us</Button>
    </Toolbar>
  </AppBar>
);

export default Navbar;
