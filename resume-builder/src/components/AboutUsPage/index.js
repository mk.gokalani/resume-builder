//  src/components/AboutUsPage/index.js

import React from 'react';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';

const AboutUsPage = () => {
  return (
    <Container>
      <Typography variant="h2" component="h1" gutterBottom>
        About Us
      </Typography>
      <Typography variant="body1" component="p">
        This is a simple resume builder app built with React, Redux, and MUI.
      </Typography>
    </Container>
  );
};

export default AboutUsPage;

