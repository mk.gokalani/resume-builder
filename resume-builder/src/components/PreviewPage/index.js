//src/components/PreviewPage/index.js

import React from 'react';
import { useSelector } from 'react-redux';
import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';

const PreviewPage = () => {
  const resume = useSelector((state) => state.resume);

  return (
    <div>
      <h1>Preview</h1>
      <div>Name: {resume.name}</div>
      <div>Email: {resume.email}</div>
      <Button variant="contained" color="primary" component={Link} to="/fill-details">
        Edit
      </Button>
    </div>
  );
};

export default PreviewPage;
