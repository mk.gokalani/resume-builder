//src/components/HomePage/index.js

import React from 'react';
import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';

const HomePage = () => (
  <div>
    <h1>Welcome to Resume Builder</h1>
    <Button variant="contained" color="primary" component={Link} to="/fill-details">
      Start Building
    </Button>
  </div>
);

export default HomePage;

