import React from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { useForm } from 'react-hook-form';
import { updateResume } from '../../redux/actions/resumeActions';

const DetailsFillingPage = () => {
  const { register, handleSubmit } = useForm();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const onSubmit = (data) => {
    dispatch(updateResume(data));
    navigate('/preview');
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <TextField {...register("name")} label="Name" variant="outlined" margin="normal" fullWidth />
      <TextField {...register("email")} label="Email" variant="outlined" margin="normal" fullWidth />
      <Button type="submit" variant="contained" color="primary">
        Next
      </Button>
    </form>
  );
};

export default DetailsFillingPage;
