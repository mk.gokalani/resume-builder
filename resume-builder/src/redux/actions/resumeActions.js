import { UPDATE_RESUME } from './types';

export const updateResume = (data) => ({
  type: UPDATE_RESUME,
  payload: data
});
