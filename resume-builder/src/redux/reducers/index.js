import { combineReducers } from '@reduxjs/toolkit';
import resume from './resumeReducer';

export default combineReducers({
  resume
});
