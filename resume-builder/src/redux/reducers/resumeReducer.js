import { UPDATE_RESUME } from '../actions/types';

const initialState = {
  name: '',
  email: ''
};

const resumeReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_RESUME:
      return {
        ...state,
        ...action.payload
      };
    default:
      return state;
  }
};

export default resumeReducer;
