// src/routes/index.js

import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import HomePage from '../components/HomePage';
import DetailsFillingPage from '../components/DetailsFillingPage';
import PreviewPage from '../components/PreviewPage';
import AboutUsPage from '../components/AboutUsPage';
import Navbar from '../components/Navbar';

const RoutesConfig = () => (
  <Router>
    <Navbar />
    <Routes>
      <Route path='/' element={<HomePage />} />
      <Route path='/fill-details' element={<DetailsFillingPage />} />
      <Route path='/preview' element={<PreviewPage />} />
      <Route path='/about-us' element={<AboutUsPage />} />
    </Routes>
  </Router>
);

export default RoutesConfig;
